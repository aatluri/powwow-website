'use strict';


powwowApp.controller('LoginController', LoginController );


                     
                     
function LoginController($scope,$location,AuthenticationService){

    $scope.login=login;
    
    AuthenticationService.clearCredentials();
    function login(){
        AuthenticationService.login($scope.username,$scope.password, authServiceLoginResponse );
        
        //Defining the authServiceLoginResponse function which is the callback function received from the AuthenticationService.login method
        function authServiceLoginResponse(response){
            if(response.success){
                AuthenticationService.setCredentials($scope.username, $scope.password);
                $location.path('/admin');
            }
            else {
                $scope.type='error';
                $scope.message=response.message;
            }
        };
    };
    
    
    
};