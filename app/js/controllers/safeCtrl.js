var powwowApp = angular.module('powwowApp');

powwowApp.controller('safeCtrl', ['$scope', function ($scope) {


    function generateRandomItem() {

        var email='adarsh.atluri@gmail.com';
        var firstName='Laurent';
        var lastName='Renard';
        var tripID=759;
        var tripType='AirLines';
        var departureCity='Mumbai';
        var arrivalCity='Vijayawada';
        var departureTime='2016-06-06 08:18:00';
        var arrivalTime='2016-06-06 08:18:00';

        return {
            email: email,
            firstName: firstName,
            lastName: lastName,
            tripID: tripID,
            tripType: tripType,
            departureCity: departureCity,
            arrivalCity: arrivalCity,
            departureTime: departureTime,
            arrivalTime: arrivalTime
        }
    }

    $scope.rowCollection = [
        {
             email:'adarsh.atluri@gmail.com'
            ,firstName:'Laurent'
            ,lastName:'Renard'
            ,tripID: 759
            ,tripType:'AirLines'
            ,departureCity:'Mumbai'
            , arrivalCity:'Vijayawada'
            ,departureTime:'2016-06-06 08:18:00'
            ,arrivalTime:'2016-06-06 08:18:00'
        }
    ];

   // for (id; id < 5; id++) {
     //   $scope.rowCollection.push(generateRandomItem(id));
    //}

    //add to the real data holder
    $scope.addRandomItem = function addRandomItem() {
        $scope.rowCollection.push(generateRandomItem());
    };

    //remove to the real data holder
    $scope.removeItem = function removeItem() {
        var index = 1;
        console.log(index);
        if (index !== -1) {
            $scope.rowCollection.splice(index, 1);
        }
    }
}]);