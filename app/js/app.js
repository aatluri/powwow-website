'use strict';
// This is where i initiate the powwowApp module. This maps to the powwowApp i mentioned in the index.html file for ng-app
// The second parameter is the other modules which my module depends on. Since i am using ng-Route here, i included the ngRoute module.
// We also need to include the angular-route java script file in the html file.
// If i want to use this module now in other places, i just need to say angular.module('powwowApp').
var powwowApp = angular.module('powwowApp',['ngRoute','ngCookies','smart-table']);
// This adds a configuration to the powwowApp module and passes the route function to it.
powwowApp.config(route);
powwowApp.run(run);

// The route function tells angular which template to inject into the index.html file based on the path. 
// The injection is done by replacing the ng-view in the html file with the respective html template
function route($routeProvider){
    $routeProvider.when('/feature',
                        {
                            templateUrl: 'templates/feature.html'
                        });
    $routeProvider.when('/explainervideo',
                        {
                            templateUrl:'templates/explainervideo.html'
                        });
    $routeProvider.when('/howitworks',
                        {
                            templateUrl:'templates/howitworks.html'
                        });
    $routeProvider.when('/team',
                        {
                            templateUrl:'templates/team.html'
                        });
    $routeProvider.when('/privacy',
                        {
                            templateUrl:'templates/privacy.html'
                        });
    $routeProvider.when('/home',
                        {
                            templateUrl:'templates/home.html'
                        });
    $routeProvider.when('/login',
                        {
                            templateUrl: 'templates/login.html',
                            controller: 'LoginController'
                        });
    $routeProvider.when('/admin',
                        {
                            templateUrl: 'templates/admin.html'
                        });
    $routeProvider.otherwise({redirectTo:'/home'});
}

// The run method is kind of like the main method in Object Oriented Programming. It is what is needed to kickstart the application
//It is executed after all of the service have been configured and the injector has been created. Run blocks typically contain code which is 
// hard to unit-test, and for this reason should be declared in isolated modules, so that they can be ignored in the unit-tests.
function run($rootScope, $location, $cookieStore){
    
    
    $rootScope.$on('$locationChangeStart', function(event, next, current){
        console.log('Entered LocationChangeStart');
        $rootScope.globals = $cookieStore.get('globals') || {};
        var restrictedPath = $.inArray($location.path(),['/admin'])===0;
        var loggedIn = $rootScope.globals.currentUser;
        if(restrictedPath && !loggedIn) {
            $location.path('/login');
        }
    });
    
    
};

