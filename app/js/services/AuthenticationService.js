'use strict';

var powwowApp = angular.module('powwowApp');

powwowApp.factory('AuthenticationService',AuthenticationService);


function AuthenticationService($timeout,$rootScope,$cookieStore){
    
    var service={};
    service.login  = login;
    service.setCredentials= setCredentials;
    service.clearCredentials = clearCredentials;
    
    return service;
    
    
    function login(username, password, callback) {
        $timeout(
            function() {
                var response;
                console.log(username);
                console.log(password);
                if (username !== null && password == 'password') {
                    response = { success: true };
                } else {
                    response = { success: false, message: 'Username or password is incorrect' };
                }
                console.log(response.success);
                callback(response);
            },1000
        );
       
    };
    
    function setCredentials(username, password) {
           // var authdata = Base64.encode(username + ':' + password);
            console.log('Entered Set Credentials');
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    password: password
                }
            };   
        $cookieStore.put('globals', $rootScope.globals);
    }
    
    
    function clearCredentials(){
        console.log('Entered Clear Credentials');
        $cookieStore.remove('globals');
    }
}