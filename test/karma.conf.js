module.exports = function (config) {
  config.set({

    basePath : '../app',

    files : [
      'lib/angular/angular.js',
      'lib/angular-resource/angular-resource.js',
      'lib/angular-route/angular-route.js',
      'lib/angular-mocks/angular-mocks.js',
      '../test/lib/sinon-1.15.0.js',
      'js/**/*.js',
      '../test/unit/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['mocha','chai'],

    browsers : ['Chrome'],

    plugins : [
      'karma-chrome-launcher',
      'karma-mocha',
      'karma-chai'
    ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};